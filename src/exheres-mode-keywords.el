;; Copyright 2009-2011 Elias Pipping <pipping@exherbo.org>
;; Distributed under the terms of the GNU General Public License v2
;; Based in part upon 'ebuild-mode-keywords.el', which is:
;;     Copyright 2006-2009 Gentoo Foundation

(eval-when-compile
  (require 'cl-lib)
  (require 'regexp-opt))

(let ((font-lock-list
       (eval-when-compile
         (let ((font-lock-common-keywords-type
                `(;; Phases, see also
                  ;; http://exherbo.org/docs/exheres-for-smarties.html#phases
                  "pkg_pretend" "pkg_nofetch" "pkg_setup" "src_unpack"
                  "src_fetch_extra" "src_prepare" "src_configure"
                  "src_compile" "src_test" "src_test_expensive"
                  "src_install" "pkg_preinst" "pkg_prerm" "pkg_postrm"
                  "pkg_postinst" "pkg_config" "pkg_info"

                  ;; Variables to modify the default behavior of those phases
                  "DEFAULT_SRC_COMPILE_PARAMS"
                  "DEFAULT_SRC_CONFIGURE_OPTION_ENABLES"
                  "DEFAULT_SRC_CONFIGURE_OPTION_WITHS"
                  "DEFAULT_SRC_CONFIGURE_PARAMS"
                  "DEFAULT_SRC_CONFIGURE_TESTS"
                  "DEFAULT_SRC_INSTALL_EXCLUDE"
                  "DEFAULT_SRC_INSTALL_EXTRA_DOCS"
                  "DEFAULT_SRC_INSTALL_EXTRA_PREFIXES"
                  "DEFAULT_SRC_INSTALL_EXTRA_SUBDIRS"
                  "DEFAULT_SRC_INSTALL_PARAMS"
                  "DEFAULT_SRC_PREPARE_PATCHES"
                  "DEFAULT_SRC_TEST_PARAMS"

                  ;; Metadata variables, see also
                  ;; http://exherbo.org/docs/exheres-for-smarties.html#metadata_variables
                  "SLOT" "DOWNLOADS" "RESTRICT" "HOMEPAGE" "LICENCES"
                  "SUMMARY" "DESCRIPTION" "DEPENDENCIES" "MYOPTIONS"
                  "REMOTE_IDS" "UPSTREAM_CHANGELOG"
                  "UPSTREAM_RELEASE_NOTES" "UPSTREAM_DOCUMENTATION"

                  ;; Environment variables, see also
                  ;; http://exherbo.org/docs/exheres-for-smarties.html#environment_variables
                  "EAPI" "REPODIR" "FETCHEDDIR" "FILES" "ARCHIVES"
                  "WORKBASE" "WORK" "IMAGE" "TEMP"

                  ;; Name and version variables, see also
                  ;; http://exherbo.org/docs/exheres-for-smarties.html#name_and_version_variables
                  "PNV" "PNVR" "PN" "PV" "PVR" "PR" "CATEGORY"

                  ;; die_functions.bash, see also
                  ;; http://exherbo.org/docs/exheres-for-smarties.html#die_functionsbash
                  "die" "assert"

                  ;; echo_functions.bash, see also
                  ;; http://exherbo.org/docs/exheres-for-smarties.html#echo_functionsbash
                  "einfo" "elog" "ewarn" "eerror" "ebegin" "eend"
                  "einfon" "ewend"

                  ;; ever_functions.bash, see also
                  ;; http://exherbo.org/docs/exheres-for-smarties.html#ever_functionsbash
                  ,@(mapcar #'(lambda (x) (concat "ever " x))
                            '("split" "split_all" "major" "range"
                              "remainder" "at_least" "delete"
                              "delete_all" "replace" "replace_all"
                              "is_scm"))

                  ;; install_functions.bash, see also
                  ;; http://exherbo.org/docs/exheres-for-smarties.html#install_functionsbash
                  "keepdir" "into" "insinto" "exeinto" "docinto"
                  "insopts" "diropts" "exeopts" "libopts"

                  ;; kernel_functions.bash, see also
                  ;; http://exherbo.org/docs/exheres-for-smarties.html#kernel_functionsbash
                  "KV_major" "KV_minor" "KV_micro" "KV_to_int"
                  "get_KV"

                  ;; sydbox.bash, see also
                  ;; http://www.exherbo.org/docs/exheres-for-smarties.html#magic_commands
                  ,@(mapcar #'(lambda (x) (concat "esandbox " x))
                            `(;; Querying sandbox status
                              "check"

                              ,@(cl-loop for l in '(("enabled")               ; Querying sandbox status
                                                    ("enable" "disable")      ; Turning sandboxing on/off
                                                    ("allow" "disallow")      ; Whitelisting
                                                    ("addfilter" "rmfilter")) ; Filtering
                                         append (cl-loop for i in l
                                                         append (cl-loop for j in '("" "_net" "_path" "_exec")
                                                                         collect (concat i j))
                                                         into list1
                                                         finally return list1)
                                         into list2
                                         finally return list2)

                              ;; Miscellaneous commands
                              "lock" "exec_lock" "wait_eldest"
                              "wait_all" "nohack_toolong" "hack_toolong"))

                  ;; exheres-0/build_functions.bash, see also
                  ;; http://exherbo.org/docs/exheres-for-smarties.html#exheres0build_functionsbash
                  "die_unless_nonfatal" "is_nonfatal"
                  "assert_unless_nonfatal" "nonfatal" "expatch"
                  "econf" "einstall" "emagicdocs" "edo"
                  "exhost"

                  ;; exheres-0/conditional_functions.bash, see also
                  ;; http://exherbo.org/docs/exheres-for-smarties.html#exheres0conditional_functionsbash
                  "option_enable" "option_with"

                  ;; exheres-0/exlib_functions.bash, see also
                  ;; http://exherbo.org/docs/exheres-for-smarties.html#exheres0exlib_functionsbash
                  "require" "default"

                  ;; exheres-0/list_functions.bash, see also
                  ;; http://exherbo.org/docs/exheres-for-smarties.html#exheres0list_functionsbash
                  "optionfmt" "option" "optionv" "optionq" "has"
                  "hasv" "hasq" "expecting_tests"

                  ;; exheres-0/output_functions.bash, see also
                  ;; http://exherbo.org/docs/exheres-for-smarties.html#exheres0output_functionsbash
                  "exdirectory --allow"
                  "exdirectory --forbid"
                  "exlogfailure"
                  "expart"
                  "exsetscmrevision"
                  "exvolatile"

                  ;; exheres-0/portage_stubs.bash, see also
                  ;; http://exherbo.org/docs/exheres-for-smarties.html#exheres0portage_stubsbash
                  "has_version" "best_version"

                  ;; utils/, see also
                  ;; http://exherbo.org/docs/exheres-for-smarties.html#utils
                  "canonicalise" "dobin" "doconfd" "dodir" "dodoc"
                  "doenvd" "doexe" "doinfo" "doinitd" "doins" "dolib"
                  "dolib.a" "dolib.so" "doman" "domo" "dosbin" "dosym"
                  "emake" "newbin" "newconfd"
                  "newdoc" "newenvd" "newexe" "newinitd" "newins"
                  "newlib.a" "newlib.so" "newman" "newsbin" "unpack"

                  ;; utils/exheres-0/, see also
                  ;; http://exherbo.org/docs/exheres-for-smarties.html#utilsexheres0
                  "nonfatal" "herebin" "hereconfd" "hereenvd"
                  "hereinitd" "hereins" "heresbin"

                  ;; easy-multibuild.exlib
                  ;; see also http://exherbo.org/docs/multibuild.html#id1
                  ,@(cl-loop for p in '("unpack" "prepare" "configure" "compile"
                                        "test" "test_expensive" "install")
                             collect (concat p "_one_multibuild")
                             collect (concat p "_prepare_one_multibuild"))

                  ;; scm.exlib -- global exheres-defined variables, see also
                  ;; http://exherbo.org/docs/exlibs/scm.html#global_exheresdefined_variables
                  "SCM_SECONDARY_REPOSITORIES"
                  "SCM_NO_PRIMARY_REPOSITORY"
                  "SCM_NO_AUTOMATIC_FINALISE"

                  ;; scm.exlib -- global scm.exlib-defined variables, see also
                  ;; http://exherbo.org/docs/exlibs/scm.html#global_defined_variables
                  "SCM_HOME"

                  ;; scm.exlib -- user-defined variables, see also
                  ;; http://exherbo.org/docs/exlibs/scm.html#userdefined_variables
                  "SCM_OFFLINE" "SCM_MIN_UPDATE_DELAY"
                  "SCM_SVN_CONFIG_DIR"

                  ;; scm.exlib -- miscellaneous variables, see also
                  ;; http://exherbo.org/docs/exlibs/scm.html#miscellaneous_variables
                  "SCM_THIS"

                  ;; scm.exlib -- exported phase functions, see also
                  ;; http://exherbo.org/docs/exlibs/scm.html#exported_phase_functions
                  "scm_src_fetch_extra" "scm_src_unpack"
                  "scm_pkg_info"

                  ;; scm.exlib -- other functions, see also
                  ;; http://exherbo.org/docs/exlibs/scm.html#other_functions
                  "scm_finalise" "scm_for_each" "scm_var_name"
                  "scm_get_var" "scm_set_var" "scm_modify_var"
                  "scm_get_array" "scm_set_array" "scm_trim_slashes"))
               (font-lock-common-keywords-warning
                '(;; exheres-0/portage_stubs.bash, see also
                  ;; http://exherbo.org/docs/exheres-for-smarties.html#exheres0portage_stubsbash
                  "portageq" "vdb_path" "check_KV" "debug-print"
                  "debug-print-function" "debug-print-section"

                  ;; utils/exheres-0/, see also
                  ;; http://exherbo.org/docs/exheres-for-smarties.html#utilsexheres0
                  "dohard" "donewins" "dosed" "dohtml" "ecompress"
                  "ecompressdir" "prepalldocs" "fowners" "fperms"

                  ;; exheres-0/conditional_functions.bash, see also
                  ;; http://exherbo.org/docs/exheres-for-smarties.html#exheres0conditional_functionsbash
                  "use_with" "use_enable"

                  ;; exheres-0/list_functions.bash, see also
                  ;; http://exherbo.org/docs/exheres-for-smarties.html#exheres0list_functionsbash
                  "use" "usev" "useq"

                  ;; exheres-0.conf: ebuild_must_not_set_variables
                  "DEPEND" "RDEPEND" "PDEPEND" "PROVIDE" "USE" "IUSE"
                  "KEYWORDS" "LICENSE" "LICENSES" "LICENCE" "SRC_URI"

                  ;; sydbox.bash, see also
                  "adddeny" "addfilter" "addpredict" "addread"
                  "addwrite" "rmfilter" "rmwrite" "sydboxcheck"
                  "sydboxcmd"

                  ;; Common typos
                  "epatch"
                  "DEFAULT_SRC_CONFIGURE_ENABLES"
                  "DEFAULT_SRC_CONFIGURE_WITHS"

                  ;; scm.exlib -- superseded by SCM_EXTERNAL_REFS
                  "SCM_SVN_EXTERNALS"))
               (font-lock-common-regexp-type-scm
                ;; scm.exlib -- per-repository variables
                (let ((regexp (regexp-opt '(;; scm.exlib-defined variables, see also
                                            ;; http://exherbo.org/docs/exlibs/scm.html#perrepository_defined_variables
                                            "ACTUAL_REVISION"
                                            "CVS_ACTUAL_REVISION_LIST"
                                            "DARCS_ACTUAL_CONTEXT"

                                            ;; Exheres-defined variables, see also
                                            ;; http://exherbo.org/docs/exlibs/scm.html#perrepository_exheresdefined_variables
                                            "BRANCH" "CHECKOUT_TO"
                                            "DARCS_CONTEXT_FILE"
                                            "EXTERNAL_REFS"
                                            "GIT_TAG_SIGNING_KEYS"
                                            "METADATA_UNNEEDED"
                                            "MTN_SEED" "REPOSITORY"
                                            "REVISION" "SUBPATH"
                                            "SVN_PASSWORD"
                                            "SVN_RAW_URI"
                                            "SVN_USERNAME""TAG" "TYPE"
                                            "UNPACK_TO"))))
                  (concat "\\<\\(SCM_\\([a-zA-Z\_]+_\\)?" regexp "\\)\\>")))
               (font-lock-exheres-keywords-type
                '(;; Metadata variables, see also
                  ;; http://exherbo.org/docs/exheres-for-smarties.html#metadata_variables
                  "PLATFORMS"))
               (font-lock-exheres-keywords-warning
                '(;; exheres-0/exlib_functions.bash, see also
                  ;; http://exherbo.org/docs/exheres-for-smarties.html#exheres0exlib_functionsbash
                  "export_exlib_phases" "myexparam" "exparam"))
               (font-lock-exlib-keywords-type
                '(;; exheres-0/exlib_functions.bash, see also
                  ;; http://exherbo.org/docs/exheres-for-smarties.html#exheres0exlib_functionsbash
                  "export_exlib_phases" "myexparam" "exparam"))
               (font-lock-exlib-keywords-warning
                '(;; exheres-0.conf: eclass_must_not_set_variables
                  "PLATFORMS")))
           (let ((font-lock-exheres-regexp-type
                  (regexp-opt (append font-lock-common-keywords-type
                                      font-lock-exheres-keywords-type)
                              'words))
                 (font-lock-exheres-regexp-warning
                  (regexp-opt (append font-lock-common-keywords-warning
                                      font-lock-exheres-keywords-warning)
                              'words))
                 (font-lock-exlib-regexp-type
                  (regexp-opt (append font-lock-common-keywords-type
                                      font-lock-exlib-keywords-type)
                              'words))
                 (font-lock-exlib-regexp-warning
                  (regexp-opt (append font-lock-common-keywords-warning
                                      font-lock-exlib-keywords-warning)
                              'words)))
             (cons
              ;; exheres-font-lock
              `((,font-lock-exheres-regexp-type 1 font-lock-type-face)
                (,font-lock-common-regexp-type-scm 1 font-lock-type-face)
                (,font-lock-exheres-regexp-warning 1 font-lock-warning-face))
              ;; exlib-font-lock
              `((,font-lock-exlib-regexp-type 1 font-lock-type-face)
                (,font-lock-common-regexp-type-scm 1 font-lock-type-face)
                (,font-lock-exlib-regexp-warning 1 font-lock-warning-face))))))))
  (defvar exheres-font-lock (car font-lock-list))
  (defvar exlib-font-lock (cdr font-lock-list)))

(provide 'exheres-mode-keywords)

;; Local Variables:
;; indent-tabs-mode: nil
;; End:
